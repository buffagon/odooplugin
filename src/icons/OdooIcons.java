package icons;

import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

/**
 * Содержит перечисление иконок плагина
 *
 * @author Прокофьев Алексей
 */
public class OdooIcons {
  public static final Icon MODULE_FILE_TYPE = IconLoader.getIcon("/icons/logo_16px.png");
  public static final Icon CONFIGURATION = IconLoader.getIcon("/icons/logo_16px.png");
  public static final Icon LOGO = IconLoader.getIcon("/icons/logo_64px.png");
}
