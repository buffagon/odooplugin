package org.buffagon.intellij.odoo.module;

import com.intellij.psi.PsiReferenceContributor;
import com.intellij.psi.PsiReferenceProvider;
import com.intellij.psi.PsiReferenceRegistrar;
import com.jetbrains.python.patterns.PythonPatterns;

/**
 * Нужен для регистрации провайдера ссылок файла-описателя модуля.
 *
 * @author Прокофьев Алексей
 */
public class OdooModuleReferenceContributor extends PsiReferenceContributor {
  @Override
  public void registerReferenceProviders(PsiReferenceRegistrar registrar) {
    PsiReferenceProvider provider = new OdooModuleReferenceProvider();
    registrar.registerReferenceProvider(PythonPatterns.pyLiteralExpression(), provider);
  }
}
