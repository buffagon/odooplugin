package org.buffagon.intellij.odoo.module;

import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiReference;
import com.intellij.psi.PsiReferenceProvider;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.util.ProcessingContext;
import com.jetbrains.python.psi.PyKeyValueExpression;
import com.jetbrains.python.psi.PyStringLiteralExpression;
import org.buffagon.intellij.odoo.OdooProjectSettingsProvider;
import org.jetbrains.annotations.NotNull;

/**
 * Провайдер ссылок для элементов файла-описателя модуля.
 *
 * @author Прокофьев Алексей
 */
public class OdooModuleReferenceProvider extends PsiReferenceProvider {
  @NotNull
  @Override
  public PsiReference[] getReferencesByElement(@NotNull PsiElement element, @NotNull ProcessingContext context) {
    if(!(element instanceof PyStringLiteralExpression))
      return PsiReference.EMPTY_ARRAY;
    if(!OdooProjectSettingsProvider.getInstance(element.getProject()).isOdooEnabled())
      return PsiReference.EMPTY_ARRAY;
    if(!OdooModuleUtil.isModuleDescriptionFile(element.getContainingFile()))
      return PsiReference.EMPTY_ARRAY;
    PyKeyValueExpression keyValueExpression = PsiTreeUtil.getParentOfType(element, PyKeyValueExpression.class);
    if(keyValueExpression == null)
      return PsiReference.EMPTY_ARRAY;
    if(OdooModuleUtil.inStringListValue(keyValueExpression, element)) {
      ModuleSections section = OdooModuleUtil.detectSection(keyValueExpression);
      if(section != null)
        return new PsiReference[]{new OdooModuleReference((PyStringLiteralExpression) element, section)};
    }
    return PsiReference.EMPTY_ARRAY;
  }
}
