package org.buffagon.intellij.odoo.module;

/**
 * Перечисление разделов файла-описателя модуля.
 *
 * @author Прокофьев Алексей
 */
public enum  ModuleSections {
  AUTHOR("author"),
  SUMMARY("summary"),
  DEPENDS("depends"),
  WEBSITE("website"),
  DATA("data"),
  DEMO("demo"),
  TEST("test"),
  INSTALLABLE("installable"),
  AUTO_INSTALL("auto_install"),
  IMAGES("images"),
  JS("js"),
  CSS("css"),
  QWEB("qweb"),
  APPLICATION("application"),
  CATEGORY("category"),
  VERSION("version"),
  INIT_XML("init_xml", true),
  UPDATE_XML("update_xml", true),
  NAME("name");

  private final String myValue;
  private final boolean myDeprecated;

  ModuleSections(String value) {
    myValue = value;
    myDeprecated = false;
  }

  ModuleSections(String value, boolean deprecated) {
    myValue = value;
    myDeprecated = deprecated;
  }

  public String geValue() {
    return myValue;
  }

  public boolean isDeprecated() {
    return myDeprecated;
  }

  public static ModuleSections fromString(String value) {
    for(ModuleSections section : ModuleSections.values())
      if(section.geValue().equals(value))
        return section;
    return null;
  }
}
