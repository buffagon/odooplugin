package org.buffagon.intellij.odoo.module;

import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.lang.annotation.Annotator;
import com.intellij.psi.PsiElement;
import com.intellij.psi.util.PsiTreeUtil;
import com.jetbrains.python.psi.PyKeyValueExpression;
import com.jetbrains.python.psi.PyStringLiteralExpression;
import org.buffagon.intellij.odoo.ReplaceLiteralExpressionQuickFix;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

/**
 * Подсвечивает устаревшие секции описателя модуля, с возможностью быстрой замены.
 *
 * @author Прокофьев Алексей
 */
public class OdooModuleDependencyAnnotator implements Annotator {
  private static final Map<String, String> deprecatedDependencies = new HashMap<String, String>();
  static {
    deprecatedDependencies.put("update_xml", "data");
    deprecatedDependencies.put("init_xml", "data");
  }

  @Override
  public void annotate(@NotNull PsiElement element, @NotNull AnnotationHolder holder) {
    if (!(element instanceof PyStringLiteralExpression))
      return;
    if(!OdooModuleUtil.isModuleDescriptionFile(element.getContainingFile()))
      return;
    PyKeyValueExpression keyValueExpression = PsiTreeUtil.getParentOfType(element, PyKeyValueExpression.class);
    if(keyValueExpression == null || !(keyValueExpression.getKey() instanceof PyStringLiteralExpression) ||
      !element.equals(keyValueExpression.getKey()))
      return;
    final String key =((PyStringLiteralExpression) keyValueExpression.getKey()).getStringValue();
    final String value = deprecatedDependencies.get(key);
    if(value != null) {
      holder.createWarningAnnotation(
        element.getTextRange(),"Section \"" + key + "\" has been deprecated. Please use \"" + value + "\" section").
        registerFix(new ReplaceLiteralExpressionQuickFix(value, ((PyStringLiteralExpression)element)));
    }
  }
}
