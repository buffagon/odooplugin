package org.buffagon.intellij.odoo.module;

import com.intellij.codeInsight.completion.*;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.openapi.project.DumbService;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.patterns.PlatformPatterns;
import com.intellij.psi.PsiFile;
import com.intellij.psi.search.FilenameIndex;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.util.ProcessingContext;
import com.jetbrains.python.psi.PyKeyValueExpression;
import com.jetbrains.python.psi.PyStringLiteralExpression;
import com.jetbrains.python.psi.search.PyProjectScopeBuilder;
import org.buffagon.intellij.odoo.OdooConstants;
import org.buffagon.intellij.odoo.OdooProjectSettingsProvider;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

/**
 * Класс предоставляет автодополнение для файла-описателя модуля.
 *
 * @author Прокофьев Алексей
 */
public class OdooModuleCompletionContributor extends CompletionContributor {
  public OdooModuleCompletionContributor() {
    extend(CompletionType.BASIC,
           PlatformPatterns.psiElement().withParent(PyStringLiteralExpression.class),
           new CompletionProvider<CompletionParameters>() {
             @Override
             protected void addCompletions(@NotNull CompletionParameters parameters, ProcessingContext context,
                                           @NotNull CompletionResultSet result) {
               final PsiFile psiFile = parameters.getOriginalFile();
               // проверяем что плагин включен
               if(!OdooProjectSettingsProvider.getInstance(psiFile.getProject()).isOdooEnabled())
                 return;
               // проверяем что не идет построение индекса
               DumbService dumbService = DumbService.getInstance(psiFile.getProject());
               if(dumbService.isDumb())
                 return;
               // проверяем что это описатель модуля
               if (!OdooModuleUtil.isModuleDescriptionFile(psiFile))
                 return;
               // получаем элемент словаря
               PyKeyValueExpression keyValueExpression =
                 PsiTreeUtil.getParentOfType(parameters.getPosition(), PyKeyValueExpression.class);
               if (keyValueExpression == null)
                 return;
               // если редактируется ключ
               if (keyValueExpression.getKey().equals(parameters.getPosition().getParent())) {
                 addKeys(result);
                 return;
               }
               // пытаемся определить текущую секцию
               ModuleSections section = OdooModuleUtil.detectSection(keyValueExpression);
               if (section == null)
                 return;
               // добавляем результаты для секции
               addCompletionForSection(keyValueExpression, section, parameters, result);
             }
           }
    );
  }

  private void addCompletionForSection(@NotNull PyKeyValueExpression keyValueExpression,
                                       @NotNull ModuleSections section,
                                       @NotNull CompletionParameters parameters,
                                       @NotNull CompletionResultSet result) {
    switch (section) {
      case DEPENDS:
        if(OdooModuleUtil.inStringListValue(keyValueExpression, parameters.getPosition().getParent()))
          addDependencies(parameters, result);
        break;
      case CSS:
        if(OdooModuleUtil.inStringListValue(keyValueExpression, parameters.getPosition().getParent()))
          addModuleFilesByExtension(result, parameters.getOriginalFile(), "css");
        break;
      case DATA:
      case INIT_XML:
      case UPDATE_XML:
        if(OdooModuleUtil.inStringListValue(keyValueExpression, parameters.getPosition().getParent()))
          addModuleFilesByExtension(result, parameters.getOriginalFile(), "xml", "csv");
        break;
      case DEMO:
        if(OdooModuleUtil.inStringListValue(keyValueExpression, parameters.getPosition().getParent()))
          addModuleFilesByExtension(result, parameters.getOriginalFile(), "xml");
        break;
      case IMAGES:
        if(OdooModuleUtil.inStringListValue(keyValueExpression, parameters.getPosition().getParent()))
          addModuleFilesByExtension(result, parameters.getOriginalFile(), "png", "jpg", "jpeg");
        break;
      case JS:
        if(OdooModuleUtil.inStringListValue(keyValueExpression, parameters.getPosition().getParent()))
          addModuleFilesByExtension(result, parameters.getOriginalFile(), "js");
        break;
      case QWEB:
        if(OdooModuleUtil.inStringListValue(keyValueExpression, parameters.getPosition().getParent()))
          addModuleFilesByExtension(result, parameters.getOriginalFile(), "xml");
        break;
    }
  }

  private void addKeys(@NotNull CompletionResultSet result) {
    for(ModuleSections section : ModuleSections.values()) {
      if(!section.isDeprecated())
        result.addElement(LookupElementBuilder.create(section.geValue()));
    }
  }

  private void addModuleFilesByExtension(CompletionResultSet result, PsiFile psiFile, String ... extensions) {
    if (psiFile.getParent() == null)
      return;
    Set<String> extensionSet = null;
    if(extensions.length > 0)
      extensionSet = new HashSet<String>(Arrays.asList(extensions));
    Stack<VirtualFile> filesStack = new Stack<VirtualFile>();
    VirtualFile rootDir = psiFile.getParent().getVirtualFile();
    filesStack.add(rootDir);
    while (!filesStack.isEmpty()) {
      VirtualFile dir = filesStack.pop();
      StringBuilder builder = new StringBuilder();
      VirtualFile currentDir = dir;
      while (currentDir != null && !currentDir.equals(rootDir)) {
        builder.append(currentDir.getName()).append("/");
        currentDir = currentDir.getParent();
      }
      String path = builder.toString();
      for (VirtualFile file : dir.getChildren()){
        if (!file.isValid())
          continue;
        if(file.isDirectory()) {
          filesStack.push(file);
          continue;
        }
        if (extensionSet == null || extensionSet.contains(file.getExtension()))
          result.addElement(LookupElementBuilder.create(path + file.getName()));
      }
    }
  }

  private void addDependencies(@NotNull CompletionParameters parameters, @NotNull CompletionResultSet result) {
    Project project = parameters.getOriginalFile().getProject();
    GlobalSearchScope scope = PyProjectScopeBuilder.excludeSdkTestsScope(project);
    for(PsiFile psiFile : FilenameIndex.getFilesByName(project, OdooConstants.ODOO_MODULE_FILENAME, scope)) {
      if (psiFile.getParent() != null)
        result.addElement(LookupElementBuilder.create(psiFile.getParent()));
    }
  }
}
