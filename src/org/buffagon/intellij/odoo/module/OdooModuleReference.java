package org.buffagon.intellij.odoo.module;

import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiManager;
import com.intellij.psi.PsiReferenceBase;
import com.jetbrains.python.psi.PyStringLiteralExpression;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Реализует ссылку на элементы указанные в файле-описателе модуля.
 *
 * @author Прокофьев Алексей
 */
public class OdooModuleReference extends PsiReferenceBase<PyStringLiteralExpression> {
  private final ModuleSections mySection;
  private final String myFilePath;

  public OdooModuleReference(@NotNull PyStringLiteralExpression
                               element, @NotNull ModuleSections section) {
    super(element);
    mySection = section;
    VirtualFile file = getElement().getContainingFile().getVirtualFile();
    myFilePath = file != null ? file.getParent().getPath()+"/"+element.getStringValue() : "";
  }

  @Nullable
  @Override
  public PsiElement resolve() {
    switch (mySection) {
      case DEPENDS:
        PsiFile file = OdooModuleUtil.getModuleFileByStringLiteralName(getElement());
        if(file != null)
          return file.getContainingDirectory();
        break;
      case CSS:
      case DATA:
      case INIT_XML:
      case UPDATE_XML:
      case DEMO:
      case IMAGES:
      case JS:
      case QWEB:
        VirtualFile virtualFile = LocalFileSystem.getInstance().findFileByPath(myFilePath);
        if(virtualFile != null)
          return PsiManager.getInstance(getElement().getProject()).findFile(virtualFile);
        break;
    }
    return null;
  }

  @NotNull
  @Override
  public Object[] getVariants() {
    return new Object[0];
  }
}
