package org.buffagon.intellij.odoo.module;
import com.intellij.openapi.fileTypes.LanguageFileType;
import com.jetbrains.python.PythonLanguage;
import icons.OdooIcons;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

/**
 * Реализация типа файла описателя модуля.
 *
 * @author Прокофьев Алексей
 */
public class OdooModuleFileType extends LanguageFileType {
  public static final LanguageFileType FILE_TYPE = new OdooModuleFileType();

  public OdooModuleFileType() {
    super(PythonLanguage.getInstance());
  }

  @NotNull
  @Override
  public String getName() {
    return "Odoo Module";
  }

  @NotNull
  @Override
  public String getDescription() {
    return "Odoo Module file";
  }

  @NotNull
  @Override
  public String getDefaultExtension() {
    return "py";
  }

  @Nullable
  @Override
  public Icon getIcon() {
    return OdooIcons.MODULE_FILE_TYPE;
  }
}
