package org.buffagon.intellij.odoo.module;

import com.intellij.openapi.fileTypes.ExactFileNameMatcher;
import com.intellij.openapi.fileTypes.FileTypeConsumer;
import com.intellij.openapi.fileTypes.FileTypeFactory;
import org.buffagon.intellij.odoo.OdooConstants;
import org.jetbrains.annotations.NotNull;

/**
 * Фабрика для типов файлов описателя модуля.
 * @see org.buffagon.intellij.odoo.module.OdooModuleFileType
 *
 * @author Прокофьев Алексей
 */
public class OdooModuleFileTypeFactory extends FileTypeFactory {

  @Override
  public void createFileTypes(@NotNull FileTypeConsumer consumer) {
    consumer.consume(OdooModuleFileType.FILE_TYPE, new ExactFileNameMatcher(OdooConstants.ODOO_MODULE_FILENAME));
  }
}
