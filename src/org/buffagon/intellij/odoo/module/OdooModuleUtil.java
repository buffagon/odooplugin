package org.buffagon.intellij.odoo.module;

import com.intellij.psi.PsiDirectory;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.search.FilenameIndex;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.util.PsiTreeUtil;
import com.jetbrains.python.psi.PyExpression;
import com.jetbrains.python.psi.PyKeyValueExpression;
import com.jetbrains.python.psi.PyListLiteralExpression;
import com.jetbrains.python.psi.PyStringLiteralExpression;
import com.jetbrains.python.psi.search.PyProjectScopeBuilder;
import org.buffagon.intellij.odoo.OdooConstants;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.LinkedList;
import java.util.List;

/**
 * Вспомогательный класс для работы с файлом-описателем модуля.
 *
 * @author Прокофьев Алексей
 * @version 1.0.0
 */
public final class OdooModuleUtil {
  /**
   * Проверить является ли psiFile файлом-описателем молуля.
   * @param psiFile файл
   * @return true если передан файл описателя, иначе false
   */
  public static boolean isModuleDescriptionFile(@NotNull PsiFile psiFile) {
    return psiFile.getName().equals(OdooConstants.ODOO_MODULE_FILENAME);
  }

  @Nullable
  public static PsiFile findModuleFileOfElement(@NotNull PsiElement element) {
    PsiFile file = (element instanceof PsiFile) ? (PsiFile) element : element.getContainingFile();
    PsiDirectory directory = file.getContainingDirectory();
    while (directory != null) {
      file = directory.findFile(OdooConstants.ODOO_MODULE_FILENAME);
      if(file != null)
        return file;
      directory = directory.getParentDirectory();
    }
    return null;
  }


  @Nullable
  public static String findModuleName(@NotNull PsiElement element) {
    PsiFile file = findModuleFileOfElement(element);
    return file == null ? null : file.getContainingDirectory().getName();
  }


  /**
   * Проверить является ли данный элемент словаря значением строкового списка.
   * @param keyValueExpression элемент словаря
   * @param element проверяемый элемент
   * @return true если является, иначе false
   */
  public static boolean inStringListValue(PyKeyValueExpression keyValueExpression, PsiElement element) {
    if(!(element instanceof PyStringLiteralExpression))
      return false;
    if(!(element.getParent() instanceof PyListLiteralExpression))
      return false;
    return keyValueExpression.equals(element.getParent().getParent());
  }

  /**
   * Получить тип элемента словаря.
   * @param keyValueExpression элемент словаря
   * @return тип если удалось опрелелить, иначе null
   */
  @Nullable
  public static ModuleSections detectSection(@NotNull PyKeyValueExpression keyValueExpression) {
    if(!(keyValueExpression.getKey() instanceof PyStringLiteralExpression))
      return null;
    return ModuleSections.fromString(((PyStringLiteralExpression) keyValueExpression.getKey()).getStringValue());
  }

  /**
   * Получить зависимости для модуля
   * @param moduleFile файл описатель модуля
   * @return список файлов описателей зависимых модулей
   */
  @Nullable
  public static List<PsiFile> getModuleDepends(PsiFile moduleFile) {
    PyKeyValueExpression dependsExpression = findModuleSection(moduleFile, ModuleSections.DEPENDS);
    if (dependsExpression == null)
      return null;
    List<PsiFile> results = new LinkedList<PsiFile>();
    if (dependsExpression.getValue() instanceof PyListLiteralExpression){
      for (PyExpression expression : ((PyListLiteralExpression) dependsExpression.getValue()).getElements()) {
        if (!(expression instanceof PyStringLiteralExpression))
          continue;
        PsiFile file = getModuleFileByStringLiteralName((PyStringLiteralExpression) expression);
        if (file != null)
          results.add(file);
      }
    }
    return results;
  }

  /**
   * Найти секцию в описателе модуля
   * @param moduleFile описатель модуля
   * @param section требуемая секция
   * @return PyKeyValueExpression если удалось найти, иначе null
   */
  @Nullable
  public static PyKeyValueExpression findModuleSection(@NotNull PsiFile moduleFile,
                                                        @NotNull ModuleSections section) {
    for(PyKeyValueExpression expression : PsiTreeUtil.findChildrenOfType(moduleFile, PyKeyValueExpression.class)) {
      ModuleSections currentSection = detectSection(expression);
      if(section.equals(currentSection))
        return expression;
    }
    return null;
  }

  /**
   * Получить файл описатель модуля по строковому элементу с наисенованием модуля
   * @param element элемент со строковым наименовнием
   * @return файл описатель модуля, null если не модуль найден
   */
  @Nullable
  public static PsiFile getModuleFileByStringLiteralName(PyStringLiteralExpression element) {
    GlobalSearchScope scope = PyProjectScopeBuilder.excludeSdkTestsScope(element.getProject());
    final PsiFile[] files =
      FilenameIndex.getFilesByName(element.getProject(), OdooConstants.ODOO_MODULE_FILENAME, scope);
    for(PsiFile file : files) {
      if (file.getParent() != null && file.getContainingDirectory().getName().equals(element.getStringValue()))
        return file;
    }
    return null;
  }
}
