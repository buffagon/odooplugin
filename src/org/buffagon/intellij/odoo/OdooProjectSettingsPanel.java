package org.buffagon.intellij.odoo;

import com.intellij.openapi.fileChooser.FileChooserDescriptor;
import com.intellij.openapi.ui.TextComponentAccessor;
import com.intellij.openapi.ui.TextFieldWithBrowseButton;
import com.intellij.openapi.util.Comparing;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;

/**
 * Форма настроек плагина.
 *
 * @author Прокофьев Алексей
 */
public class OdooProjectSettingsPanel {
  private static final String PATH_SEPARATOR = ";";
  private JPanel rootPanel;
  private JCheckBox enableOdooSupportField;
  private TextFieldWithBrowseButton configPathField;
  private TextFieldWithBrowseButton addonsPathField;

  private OdooProjectSettingsProvider mySettingsProvider;


  public OdooProjectSettingsPanel() {

    enableOdooSupportField.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        updateUI();
      }
    });


    FileChooserDescriptor fileChooserDescriptor = new FileChooserDescriptor(false, true, false, false, false, false);
    addonsPathField.addBrowseFolderListener(
      "",
      "Odoo addons path",
      null,
      fileChooserDescriptor,
      TextComponentAccessor.TEXT_FIELD_WHOLE_TEXT,
      false
    );

    fileChooserDescriptor = new FileChooserDescriptor(true, false, false, false, false, false);
    configPathField.addBrowseFolderListener(
      "",
      "Odoo config path",
      null,
      fileChooserDescriptor,
      TextComponentAccessor.TEXT_FIELD_WHOLE_TEXT,
      false
    );
  }

  private void updateUI() {
    addonsPathField.setEnabled(enableOdooSupportField.isSelected());
    configPathField.setEnabled(enableOdooSupportField.isSelected());
  }

  public JComponent getPanel(OdooProjectSettingsProvider provider) {
    mySettingsProvider = provider;
    return rootPanel;
  }

  public boolean isModified() {
    return !(Comparing.equal(mySettingsProvider.getConfigPath(), configPathField.getText()) &&
           Comparing.equal(mySettingsProvider.isOdooEnabled(), enableOdooSupportField.isSelected()) &&
           Comparing.equal(mySettingsProvider.getAddonsPathList(), splitAddonsPaths(addonsPathField.getText())));
  }

  public void apply() {
    mySettingsProvider.setConfigPath(configPathField.getText());
    mySettingsProvider.setOdooEnabled(enableOdooSupportField.isSelected());
    mySettingsProvider.setAddonsPathList(splitAddonsPaths(addonsPathField.getText()));
  }

  public void reset() {
    enableOdooSupportField.setSelected(mySettingsProvider.isOdooEnabled());
    configPathField.setText(mySettingsProvider.getConfigPath());
    addonsPathField.setText(joinAddonsPaths(mySettingsProvider.getAddonsPathList()));
    updateUI();
  }

  private List<String> splitAddonsPaths(String paths) {
    List<String> list = new LinkedList<String>();
    for(String s : paths.split(PATH_SEPARATOR)) {
      if(!s.isEmpty())
        list.add(s);
    }
    return list;
  }


  private String joinAddonsPaths(List<String> paths) {
    String addonsPath = "";
    for(String s: paths)
      addonsPath += s + PATH_SEPARATOR;
    return addonsPath;
  }
}
