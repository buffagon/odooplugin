package org.buffagon.intellij.odoo.xml;

import com.intellij.openapi.util.Condition;
import com.intellij.psi.PsiElement;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.psi.xml.*;

/**
 * Класс для работы с odoo-xml
 * <p/>
 * Created by lesik_ia on 27.11.14.
 */
public final class OdooXmlUtil {
  /**
   * Является ли psiElement значением атрибута name тэга field,
   * лежащего внутри секции формы (<field name="arch" type="xml">).
   */
  public static boolean isFieldNameAndInsideArch(PsiElement psiElement) {
    if (!(psiElement instanceof XmlToken))
      return false;

    XmlAttribute xmlAttribute = PsiTreeUtil.getParentOfType(psiElement, XmlAttribute.class);
    if (xmlAttribute == null || !xmlAttribute.getName().equals("name"))
      return false;

    XmlTag xmlTag = PsiTreeUtil.getParentOfType(xmlAttribute, XmlTag.class);
    if (xmlTag == null || !xmlTag.getName().equals("field"))
      return false;

    return haveParentFieldWithNameArch(xmlTag);
  }

  /**
   * Возвращает название модели, используемой во view
   */
  public static String getModelNameByPosition(PsiElement psiElement) {
    PsiElement record = PsiTreeUtil.findFirstParent(psiElement, new Condition<PsiElement>() {
      @Override
      public boolean value(PsiElement psiElement) {
        return psiElement instanceof XmlTag && ((XmlTag) psiElement).getName().equals("record");
      }
    });

    if (record == null) {
      return null;
    }

    XmlTag[] xmlTags = PsiTreeUtil.getChildrenOfType(record, XmlTag.class);
    if (xmlTags != null) {
      for (XmlTag xmlTag : xmlTags) {
        if (!xmlTag.getName().equals("field"))
          continue;
        String tempText = null;
        boolean isModelName = false;

        XmlAttribute[] xmlAttributes = PsiTreeUtil.getChildrenOfType(xmlTag, XmlAttribute.class);
        if (xmlAttributes != null) {
          for (XmlAttribute xmlAttribute : xmlAttributes) {
            if (xmlAttribute.getName().equals("name") && ("model".equals(xmlAttribute.getValue())))
              isModelName = true;
          }
        }

        XmlText[] xmlTexts = PsiTreeUtil.getChildrenOfType(xmlTag, XmlText.class);
        if (xmlTexts != null) {
          for (XmlText xmlText : xmlTexts)
            tempText = xmlText.getValue();
        }

        if (isModelName)
          return tempText;
      }
    }
    return null;
  }


  private static boolean haveParentFieldWithNameArch(PsiElement psiElement) {
    if (psiElement == null)
      return false;

    if (psiElement instanceof XmlTag) {
      if (((XmlTag) psiElement).getName().equals("field")) {
        String name = ((XmlTag) psiElement).getAttributeValue("name");
        if("arch".equals(name))
          return true;
      }
    }
    return haveParentFieldWithNameArch(psiElement.getParent());
  }

}
