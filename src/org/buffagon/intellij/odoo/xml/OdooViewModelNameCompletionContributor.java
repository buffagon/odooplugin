package org.buffagon.intellij.odoo.xml;

import com.intellij.codeInsight.completion.*;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.ide.highlighter.XmlFileType;
import com.intellij.openapi.project.Project;
import com.intellij.patterns.PlatformPatterns;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.xml.XmlAttributeValue;
import com.intellij.psi.xml.XmlText;
import com.intellij.util.ProcessingContext;
import org.buffagon.intellij.odoo.OdooProjectSettingsProvider;
import org.buffagon.intellij.odoo.model.OdooModelNameIndex;
import org.jetbrains.annotations.NotNull;

/**
 * lesik_ia on 13.11.14.
 */
public class OdooViewModelNameCompletionContributor extends CompletionContributor {

  public OdooViewModelNameCompletionContributor() {
    extend(CompletionType.BASIC,
           PlatformPatterns.psiElement().withParent(XmlText.class),
           new CompletionProvider<CompletionParameters>() {
             @Override
             protected void addCompletions(@NotNull CompletionParameters parameters, ProcessingContext context,
                                           @NotNull CompletionResultSet result) {
               final PsiFile psiFile = parameters.getOriginalFile();
               if (!OdooProjectSettingsProvider.getInstance(psiFile.getProject()).isOdooEnabled())
                 return;
               if (!psiFile.getFileType().equals(XmlFileType.INSTANCE))
                 return;
               if (OdooModelReferenceProvider.isModelField(parameters.getPosition()))
                 addModelNames(parameters.getOriginalPosition(), result, psiFile.getProject());
             }
           }
    );

    extend(CompletionType.BASIC,
           PlatformPatterns.psiElement().withParent(XmlAttributeValue.class),
           new CompletionProvider<CompletionParameters>() {
             @Override
             protected void addCompletions(@NotNull CompletionParameters parameters, ProcessingContext context,
                                           @NotNull CompletionResultSet result) {
               final PsiFile psiFile = parameters.getOriginalFile();
               if (!OdooProjectSettingsProvider.getInstance(psiFile.getProject()).isOdooEnabled())
                 return;
               if (!psiFile.getFileType().equals(XmlFileType.INSTANCE))
                 return;

               if (OdooXmlUtil.isFieldNameAndInsideArch(parameters.getPosition())) {
                 String modelName = OdooXmlUtil.getModelNameByPosition(parameters.getPosition());
                 addModelColumns(parameters.getOriginalPosition(), result, modelName);
               }
             }
           }
    );
  }

  private void addModelNames(PsiElement reference, CompletionResultSet result, Project project) {
    for (String moduleId : OdooModelNameIndex.findModelNames(reference, project, true))
      result.addElement(LookupElementBuilder.create(moduleId));
  }

  private void addModelColumns(PsiElement reference, CompletionResultSet result, String modelName) {
    for (String column : OdooModelNameIndex.findModelColumns(reference, modelName, true)) {
      result.addElement(LookupElementBuilder.create(column));
    }
  }
}
