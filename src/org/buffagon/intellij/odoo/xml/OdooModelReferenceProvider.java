package org.buffagon.intellij.odoo.xml;

import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiReference;
import com.intellij.psi.PsiReferenceProvider;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.psi.xml.XmlTag;
import com.intellij.psi.xml.XmlToken;
import com.intellij.util.ProcessingContext;
import org.jetbrains.annotations.NotNull;

/**
 * lesik_ia on 12.11.14.
 */
public class OdooModelReferenceProvider extends PsiReferenceProvider {
  @NotNull
  @Override
  public PsiReference[] getReferencesByElement(@NotNull PsiElement element, @NotNull ProcessingContext context) {
    if (!isModelField(element))
      return PsiReference.EMPTY_ARRAY;
    PsiReference ref = new OdooModelNameReference(element, element.getText());
    return new PsiReference[]{ref};
  }

  public static boolean isModelField(@NotNull PsiElement element) {
    if (!(element instanceof XmlToken) || element.getParent() == null || element.getParent().getParent() == null)
      return false;

    PsiElement field = element.getParent().getParent();
    if (!(field instanceof XmlTag))
      return false;

    for (PsiElement fieldChild : field.getChildren()) {
      if (!(fieldChild instanceof XmlAttribute))
        continue;
      XmlAttribute nameAttribute = (XmlAttribute) fieldChild;
      if(nameAttribute.getDescriptor() == null || nameAttribute.getDisplayValue() == null)
        continue;
      if (nameAttribute.getDescriptor().getName().equals("name") && nameAttribute.getDisplayValue().equals("model"))
        return true;
    }
    return false;
  }
}
