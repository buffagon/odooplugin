package org.buffagon.intellij.odoo.xml;

import com.intellij.patterns.StandardPatterns;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiReferenceContributor;
import com.intellij.psi.PsiReferenceRegistrar;
import org.jetbrains.annotations.NotNull;

/**
 * lesik_ia on 12.11.14.
 */
public class OdooViewReferenceContributor extends PsiReferenceContributor {
  @Override
  public void registerReferenceProviders(@NotNull PsiReferenceRegistrar registrar) {
    OdooModelReferenceProvider provider = new OdooModelReferenceProvider();
    registrar.registerReferenceProvider(StandardPatterns.instanceOf(PsiElement.class), provider);
  }
}
