package org.buffagon.intellij.odoo.xml;

import com.intellij.openapi.util.text.StringUtil;
import com.intellij.psi.*;
import com.intellij.util.ReflectionUtil;
import com.jetbrains.python.psi.PyTargetExpression;
import org.buffagon.intellij.odoo.model.OdooModelNameIndex;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;

/**
 * lesik_ia on 12.11.14.
 */
public class OdooModelNameReference extends PsiReferenceBase<PsiElement> implements PsiPolyVariantReference, ResolvingHint {
  private final String myBundleName;
  private final String modelName;
  private final PsiElement element;

  public OdooModelNameReference(final PsiElement element, String modelName) {
    this(element, modelName, false);
  }

  public OdooModelNameReference(final PsiElement element, String modelName, boolean soft) {
    super(element, soft);
    myBundleName = StringUtil.replaceChar(getValue(), '/', '.');
    this.modelName = modelName;
    this.element = element;
  }

  @Override
  public boolean canResolveTo(Class<? extends PsiElement> elementClass) {
    return ReflectionUtil.isAssignable(PsiFile.class, elementClass);
  }

  @Nullable
  @Override
  public PsiElement resolve() {
    return null;
  }

  @NotNull
  @Override
  public ResolveResult[] multiResolve(final boolean incompleteCode) {
    final ArrayList<ResolveResult> resolveResults = new ArrayList<ResolveResult>();
    Collection<PyTargetExpression> expressions = OdooModelNameIndex.find(element, modelName, true);
    for (PyTargetExpression pyTargetExpression : expressions)
      resolveResults.add(new PsiElementResolveResult(pyTargetExpression));
    return resolveResults.toArray(new ResolveResult[resolveResults.size()]);
  }

  @NotNull
  @Override
  public String getCanonicalText() {
    return myBundleName;
  }

  @Override
  public boolean isReferenceTo(PsiElement element) {
    return false;
  }


  @NotNull
  @Override
  public Object[] getVariants() {
    return new Object[]{};
  }
}
