package org.buffagon.intellij.odoo;

import com.intellij.codeInsight.FileModificationService;
import com.intellij.codeInsight.intention.impl.BaseIntentionAction;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiFile;
import com.intellij.util.IncorrectOperationException;
import com.jetbrains.python.psi.PyElementGenerator;
import com.jetbrains.python.psi.PyStringLiteralExpression;
import org.jetbrains.annotations.NotNull;

/**
 * Позволяет заменить значение текстового блока на другое.
 *
 * @author Прокофьев Алексей
 */
public class ReplaceLiteralExpressionQuickFix extends BaseIntentionAction {
  private final String myValue;
  private final PyStringLiteralExpression myElement;
  private String myText = "Replace";

  public ReplaceLiteralExpressionQuickFix(String value, PyStringLiteralExpression element) {
    this.myValue = value;
    this.myElement = element;
  }

  @NotNull
  @Override
  public String getText() {
    return myText;
  }

  public void setText(@NotNull String text) {
    this.myText = text;
  }

  @NotNull
  @Override
  public String getFamilyName() {
    return "Odoo module";
  }

  @Override
  public boolean isAvailable(@NotNull Project project, Editor editor, PsiFile file) {
    return true;
  }

  @Override
  public void invoke(@NotNull final Project project, Editor editor, final PsiFile file) throws IncorrectOperationException {
    if (!FileModificationService.getInstance().prepareFileForWrite(file)) return;
    myElement.replace(PyElementGenerator.getInstance(project).createStringLiteralFromString(myValue));
  }
}
