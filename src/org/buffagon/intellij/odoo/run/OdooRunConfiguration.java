package org.buffagon.intellij.odoo.run;

import com.intellij.execution.ExecutionException;
import com.intellij.execution.Executor;
import com.intellij.execution.configurations.ConfigurationFactory;
import com.intellij.execution.configurations.RunProfileState;
import com.intellij.execution.runners.ExecutionEnvironment;
import com.intellij.openapi.components.PathMacroManager;
import com.intellij.openapi.options.SettingsEditor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.InvalidDataException;
import com.intellij.openapi.util.JDOMExternalizerUtil;
import com.intellij.openapi.util.WriteExternalException;
import com.intellij.openapi.util.text.StringUtil;
import com.jetbrains.python.run.AbstractPythonRunConfiguration;
import com.jetbrains.python.run.AbstractPythonRunConfigurationParams;
import org.buffagon.intellij.odoo.OdooConstants;
import org.jdom.Element;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Конфигурация для запуска Odoo сервера.
 *
 * @author Прокофьев Алексей
 */
public class OdooRunConfiguration extends AbstractPythonRunConfiguration
  implements AbstractPythonRunConfigurationParams, OdooRunConfigurationParams{

  private static final String PG_PATH = "PG_PATH";
  private static final String PORT = "PORT";
  private static final String DB_HOST = "DB_HOST";
  private static final String DB_PORT = "DB_PORT";
  private static final String DB_USER = "DB_USER";
  private static final String DB_PASSWORD = "DB_PASSWORD";
  private static final String DB_NAME = "DB_NAME";
  private static final String LOG_LEVEL = "LOG_LEVEL";
  private static final String DEBUG_MODE = "DEBUG_MODE";
  private static final String LOAD_DEMO = "LOAD_DEMO";
  private static final String ADDITIONAL_PARAMS = "ADDITIONAL_PARAMS";


  private String myPgPath;
  private int myPort = OdooConstants.DEFAULT_SERVER_PORT;
  private String myDbHost = "localhost";
  private int myDbPort = OdooConstants.DEFAULT_DB_PORT;
  private String myDbUser = "postgres";
  private String myDbPassword;
  private String myDbName = "odoo_db";
  private String myLogLevel = "info";
  private boolean myDebugMode = false;
  private boolean myLoadDemo = true;
  private String myAdditionalParams;


  public OdooRunConfiguration(Project project, ConfigurationFactory factory) {
    super(project, factory);
  }

  @Override
  public String getPgPath() {
    return myPgPath;
  }

  @Override
  public void setPgPath(String path) {
    this.myPgPath = path;
  }

  @Override
  public int getPort() {
    return myPort;
  }

  @Override
  public void setPort(int port) {
    this.myPort = port;
  }

  @Override
  public String getDbHost() {
    return myDbHost;
  }

  @Override
  public void setDbHost(String dbHost) {
    this.myDbHost = dbHost;
  }

  @Override
  public int getDbPort() {
    return myDbPort;
  }

  @Override
  public void setDbPort(int dbPort) {
    this.myDbPort = dbPort;
  }

  @Override
  public String getDbUser() {
    return myDbUser;
  }

  @Override
  public void setDbUser(String dbUser) {
    this.myDbUser = dbUser;
  }

  @Override
  public String getDbPassword() {
    return myDbPassword;
  }

  @Override
  public void setDbPassword(String dbPassword) {
    this.myDbPassword = dbPassword;
  }

  @Override
  public String getDbName() {
    return myDbName;
  }

  @Override
  public void setDbName(String dbName) {
    this.myDbName = dbName;
  }

  @Override
  public String getLogLevel() {
    return myLogLevel;
  }

  @Override
  public void setLogLevel(String logLevel) {
    this.myLogLevel = logLevel;
  }

  @Override
  public boolean isDebugMode() {
    return myDebugMode;
  }

  @Override
  public void setDebugMode(boolean debugMode) {
    this.myDebugMode = debugMode;
  }

  @Override
  public boolean isLoadDemo() {
    return myLoadDemo;
  }

  @Override
  public void setLoadDemo(boolean loadDemo) {
    this.myLoadDemo = loadDemo;
  }

  @Override
  public String getAdditionalParams() {
    return myAdditionalParams;
  }

  @Override
  public void setAdditionalParams(String params) {
    this.myAdditionalParams = params;
  }

  @Nullable
  public String getScriptName() {
    String path = getInterpreterPath();
    if(path == null)
      return null;

    int index = path.replace("\\", "/").lastIndexOf("/");
    if(index == -1)
      return null;
    return path.substring(0, index) + "/openerp-server" ;
  }

  @NotNull
  public String getScriptParameters() {
    StringBuilder builder = new StringBuilder();
    builder
      .append(" --database=").append(myDbName)
      .append(" --db_user=").append(myDbUser)
      .append(" --db_password=").append(myDbPassword)
      .append(" --db_host=").append(myDbHost)
      .append(" --xmlrpc-port=").append(myPort)
      .append(" --log-level=").append(myLogLevel);

    if(!myLoadDemo)
      builder.append(" --without-demo=all");
    if(myDebugMode)
      builder.append(" --debug");
    if(!StringUtil.isEmptyOrSpaces(myPgPath))
      builder.append(" --pg_path=").append(myPgPath);
    builder.append(" --addons-path=\"").append(getProject().getBasePath()).append("\"");
    if(!StringUtil.isEmptyOrSpaces(myAdditionalParams))
      builder.append(" ").append(myAdditionalParams);

    return builder.toString();
  }

  public static void copyParams(OdooRunConfigurationParams source, OdooRunConfigurationParams target) {
    AbstractPythonRunConfiguration.copyParams(source.getBaseParams(), target.getBaseParams());
    target.setPgPath(source.getPgPath());
    target.setPort(source.getPort());
    target.setDbHost(source.getDbHost());
    target.setDbPort(source.getDbPort());
    target.setDbUser(source.getDbUser());
    target.setDbPassword(source.getDbPassword());
    target.setDbName(source.getDbName());
    target.setLogLevel(source.getLogLevel());
    target.setDebugMode(source.isDebugMode());
    target.setLoadDemo(source.isLoadDemo());
    target.setAdditionalParams(source.getAdditionalParams());
  }

  @Override
  protected SettingsEditor createConfigurationEditor() {
    return new OdooRunConfigurationEditor(this);
  }

  @Nullable
  @Override
  public RunProfileState getState(@NotNull Executor executor, @NotNull ExecutionEnvironment env) throws ExecutionException {
    return new OdooCommandLineState(this, env);
  }

  @Override
  public AbstractPythonRunConfigurationParams getBaseParams() {
    return this;
  }

  public void readExternal(Element element) throws InvalidDataException {
    PathMacroManager.getInstance(getProject()).expandPaths(element);
    super.readExternal(element);
    myPgPath = JDOMExternalizerUtil.readField(element, PG_PATH);
    myPort = Integer.parseInt(JDOMExternalizerUtil.readField(
      element, PORT, String.valueOf(OdooConstants.DEFAULT_SERVER_PORT)));
    myDbHost = JDOMExternalizerUtil.readField(element, DB_HOST);
    myDbPort = Integer.parseInt(JDOMExternalizerUtil.readField(
      element, DB_PORT, String.valueOf(OdooConstants.DEFAULT_DB_PORT)));
    myDbUser = JDOMExternalizerUtil.readField(element, DB_USER);
    myDbPassword = JDOMExternalizerUtil.readField(element, DB_PASSWORD);
    myDbName = JDOMExternalizerUtil.readField(element, DB_NAME);
    myLogLevel = JDOMExternalizerUtil.readField(element, LOG_LEVEL);
    myDebugMode = Boolean.parseBoolean(JDOMExternalizerUtil.readField(element, DEBUG_MODE, "false"));
    myLoadDemo = Boolean.parseBoolean(JDOMExternalizerUtil.readField(element, LOAD_DEMO, "false"));
    myAdditionalParams = JDOMExternalizerUtil.readField(element, ADDITIONAL_PARAMS);
  }

  public void writeExternal(Element element) throws WriteExternalException {
    super.writeExternal(element);
    JDOMExternalizerUtil.writeField(element, PG_PATH, myPgPath);
    JDOMExternalizerUtil.writeField(element, PORT, String.valueOf(myPort));
    JDOMExternalizerUtil.writeField(element, DB_HOST, myDbHost);
    JDOMExternalizerUtil.writeField(element, DB_PORT, String.valueOf(myDbPort));
    JDOMExternalizerUtil.writeField(element, DB_USER, myDbUser);
    JDOMExternalizerUtil.writeField(element, DB_PASSWORD, myDbPassword);
    JDOMExternalizerUtil.writeField(element, DB_NAME, myDbName);
    JDOMExternalizerUtil.writeField(element, LOG_LEVEL, myLogLevel);
    JDOMExternalizerUtil.writeField(element, DEBUG_MODE, String.valueOf(myDebugMode));
    JDOMExternalizerUtil.writeField(element, LOAD_DEMO, String.valueOf(myLoadDemo));
    JDOMExternalizerUtil.writeField(element, ADDITIONAL_PARAMS, myAdditionalParams);
    PathMacroManager.getInstance(getProject()).collapsePathsRecursively(element);
  }
}
