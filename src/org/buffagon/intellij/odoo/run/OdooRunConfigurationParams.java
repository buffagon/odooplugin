package org.buffagon.intellij.odoo.run;

import com.jetbrains.python.run.AbstractPythonRunConfigurationParams;

/**
 * Интерфейс со списком параметров конфигурации.
 *
 * @author Прокофьев Алексей
 */
public interface OdooRunConfigurationParams {
  AbstractPythonRunConfigurationParams getBaseParams();

  String getPgPath();
  void setPgPath(String password);

  int getPort();
  void setPort(int port);

  String getDbHost();
  void setDbHost(String host);

  int getDbPort();
  void setDbPort(int port);

  String getDbUser();
  void setDbUser(String user);

  String getDbPassword();
  void setDbPassword(String password);

  String getDbName();
  void setDbName(String dbName);

  String getLogLevel();
  void setLogLevel(String level);

  boolean isDebugMode();
  void setDebugMode(boolean enabled);

  boolean isLoadDemo();
  void setLoadDemo(boolean enabled);

  String getAdditionalParams();
  void setAdditionalParams(String params);
}
