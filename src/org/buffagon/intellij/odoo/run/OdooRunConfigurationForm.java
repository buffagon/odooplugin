package org.buffagon.intellij.odoo.run;

import com.intellij.openapi.fileChooser.FileChooserDescriptor;
import com.intellij.openapi.ui.TextComponentAccessor;
import com.intellij.openapi.ui.TextFieldWithBrowseButton;
import com.jetbrains.python.run.AbstractPyCommonOptionsForm;
import com.jetbrains.python.run.AbstractPythonRunConfigurationParams;
import com.jetbrains.python.run.PyCommonOptionsFormFactory;
import org.buffagon.intellij.odoo.OdooConstants;

import javax.swing.*;

/**
 * Класс формы настроек.
 *
 * @author Прокофьев Алексей
 */
public class OdooRunConfigurationForm implements OdooRunConfigurationParams {
  private final AbstractPyCommonOptionsForm myCommonOptionsForm;
  private JPanel rootPanel;
  private JTextField dbHostTextField;
  private JTextField dbUserTextField;
  private JTextField dbNameTextField;
  private JPasswordField dbPasswordField;
  private JComboBox logLevelcomboBox;
  private JCheckBox demoCheckBox;
  private JSpinner dbPortSpinner;
  private JSpinner portSpinner;
  private JCheckBox debugModeCheckBox;
  private TextFieldWithBrowseButton pgPathTextFieldWithBrowseButton;
  private JTextField additionalParamsTextField;

  public OdooRunConfigurationForm(OdooRunConfiguration configuration) {
    myCommonOptionsForm = PyCommonOptionsFormFactory.getInstance().createForm(configuration.getCommonOptionsFormData());

    dbPortSpinner.setModel(new SpinnerNumberModel(OdooConstants.DEFAULT_DB_PORT,1,65535,1));
    dbPortSpinner.setEditor(new JSpinner.NumberEditor(dbPortSpinner, "#####"));

    portSpinner.setModel(new SpinnerNumberModel(OdooConstants.DEFAULT_SERVER_PORT,1,65535,1));
    portSpinner.setEditor(new JSpinner.NumberEditor(portSpinner, "#####"));

    FileChooserDescriptor fileChooserDescriptor = new FileChooserDescriptor(true, false, false, false, false, false);
    pgPathTextFieldWithBrowseButton.addBrowseFolderListener(
      "Select pg_config file",
      "",
      configuration.getProject(),
      fileChooserDescriptor,
      TextComponentAccessor.TEXT_FIELD_WHOLE_TEXT,
      false
    );
  }

  @Override
  public AbstractPythonRunConfigurationParams getBaseParams() {
    return myCommonOptionsForm;
  }

  @Override
  public String getPgPath() {
    return String.valueOf(pgPathTextFieldWithBrowseButton.getText());
  }

  @Override
  public void setPgPath(String path) {
    pgPathTextFieldWithBrowseButton.setText(path);
  }

  @Override
  public int getPort() {
    return  (Integer) portSpinner.getValue();
  }

  @Override
  public void setPort(int port) {
    portSpinner.setValue(port);
  }

  @Override
  public String getDbHost() {
    return dbHostTextField.getText();
  }

  @Override
  public void setDbHost(String host) {
    dbHostTextField.setText(host);
  }

  @Override
  public int getDbPort() {
    return (Integer) dbPortSpinner.getValue();
  }

  @Override
  public void setDbPort(int port) {
    dbPortSpinner.setValue(port);
  }

  @Override
  public String getDbUser() {
    return dbUserTextField.getText();
  }

  @Override
  public void setDbUser(String user) {
    dbUserTextField.setText(user);
  }

  @Override
  public String getDbPassword() {
    return String.valueOf(dbPasswordField.getPassword());
  }

  @Override
  public void setDbPassword(String password) {
    dbPasswordField.setText(password);
  }

  @Override
  public String getDbName() {
    return dbNameTextField.getText();
  }

  @Override
  public void setDbName(String dbName) {
    dbNameTextField.setText(dbName);
  }

  @Override
  public String getLogLevel() {
    return (String) logLevelcomboBox.getSelectedItem();
  }

  @Override
  public void setLogLevel(String level) {
    logLevelcomboBox.setSelectedItem(level);
  }

  @Override
  public boolean isDebugMode() {
    return debugModeCheckBox.isSelected();
  }

  @Override
  public void setDebugMode(boolean enabled) {
    debugModeCheckBox.setSelected(enabled);
  }

  @Override
  public boolean isLoadDemo() {
    return demoCheckBox.isSelected();
  }

  @Override
  public void setLoadDemo(boolean enabled) {
    demoCheckBox.setSelected(enabled);
  }

  @Override
  public String getAdditionalParams() {
    return additionalParamsTextField.getText();
  }

  @Override
  public void setAdditionalParams(String params) {
    additionalParamsTextField.setText(params);
  }

  public JPanel getPanel() {
    return rootPanel;
  }
}
