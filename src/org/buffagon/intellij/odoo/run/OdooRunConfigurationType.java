package org.buffagon.intellij.odoo.run;

import com.intellij.execution.configuration.ConfigurationFactoryEx;
import com.intellij.execution.configurations.ConfigurationFactory;
import com.intellij.execution.configurations.ConfigurationType;
import com.intellij.execution.configurations.RunConfiguration;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.project.Project;
import icons.OdooIcons;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

/**
 * Регистрирует тип конфигурации запуска для Odoo сервера.
 *
 * @author Прокофьев Алексей
 */
public class OdooRunConfigurationType implements ConfigurationType {
  private static final Logger LOGGER = Logger.getInstance("Odoo Runner");
  private final ConfigurationFactory myFactory;

  public OdooRunConfigurationType() {
    myFactory = new ConfigurationFactoryEx(this)
    {
      @Override
      public RunConfiguration createTemplateConfiguration(Project project) {
        LOGGER.info("Create Odoo Template Configuration");
        return new OdooRunConfiguration(project, this);
      }
    };
  }

  @Override
  public String getDisplayName() {
    return "Odoo";
  }

  @Override
  public String getConfigurationTypeDescription() {
    return "Odoo Configuration";
  }

  @Override
  public Icon getIcon() {
    return OdooIcons.CONFIGURATION;
  }

  @NotNull
  @Override
  public String getId() {
    return "Odoo";
  }

  @Override
  public ConfigurationFactory[] getConfigurationFactories() {
    return new ConfigurationFactory[] {myFactory};
  }
}
