package org.buffagon.intellij.odoo.run;

import com.intellij.execution.configurations.GeneralCommandLine;
import com.intellij.execution.configurations.ParametersList;
import com.intellij.execution.configurations.ParamsGroup;
import com.intellij.execution.runners.ExecutionEnvironment;
import com.intellij.openapi.util.text.StringUtil;
import com.jetbrains.python.run.PythonCommandLineState;

/**
 * Подготавливает параметры командной строки для запуска конфигурации.
 *
 * @author Прокофьев Алексей
 */
public class OdooCommandLineState extends PythonCommandLineState {
  private final OdooRunConfiguration myConfig;

  public OdooCommandLineState(OdooRunConfiguration configuration, ExecutionEnvironment env) {
    super(configuration, env);
    this.myConfig = configuration;
  }

  @Override
  protected void buildCommandLineParameters(GeneralCommandLine commandLine) {
    ParametersList parametersList = commandLine.getParametersList();
    ParamsGroup exe_options = parametersList.getParamsGroup(GROUP_EXE_OPTIONS);
    assert exe_options != null;
    exe_options.addParametersString(myConfig.getInterpreterOptions());

    ParamsGroup script_parameters = parametersList.getParamsGroup(GROUP_SCRIPT);
    assert script_parameters != null;
    final String scriptName = myConfig.getScriptName();
    if(scriptName != null)
      script_parameters.addParameter(scriptName);
    script_parameters.addParametersString(myConfig.getScriptParameters());

    if (!StringUtil.isEmptyOrSpaces(myConfig.getWorkingDirectory())) {
      commandLine.setWorkDirectory(myConfig.getWorkingDirectory());
    }
  }
}
