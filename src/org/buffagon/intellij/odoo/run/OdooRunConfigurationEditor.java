package org.buffagon.intellij.odoo.run;

import com.intellij.openapi.options.ConfigurationException;
import com.intellij.openapi.options.SettingsEditor;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

/**
 * Редактор конфигурации.
 *
 * @author Прокофьев Алексей
 */
public class OdooRunConfigurationEditor extends SettingsEditor<OdooRunConfiguration> {
  private OdooRunConfigurationForm myForm;
  public OdooRunConfigurationEditor(OdooRunConfiguration configuration) {
    myForm = new OdooRunConfigurationForm(configuration);
  }

  @Override
  protected void resetEditorFrom(OdooRunConfiguration config) {
    OdooRunConfiguration.copyParams(config, myForm);
  }

  @Override
  protected void applyEditorTo(OdooRunConfiguration config) throws ConfigurationException {
    OdooRunConfiguration.copyParams(myForm, config);
  }

  @NotNull
  @Override
  protected JComponent createEditor() {
    return myForm.getPanel();
  }

  @Override
  protected void disposeEditor() {
    myForm = null;
  }
}
