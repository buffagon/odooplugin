package org.buffagon.intellij.odoo.actions;

import com.intellij.codeInsight.FileModificationService;
import com.intellij.ide.IdeView;
import com.intellij.ide.actions.CreateDirectoryOrPackageHandler;
import com.intellij.ide.fileTemplates.FileTemplate;
import com.intellij.ide.fileTemplates.FileTemplateManager;
import com.intellij.ide.fileTemplates.FileTemplateUtil;
import com.intellij.ide.highlighter.XmlFileType;
import com.intellij.ide.util.DirectoryChooserUtil;
import com.intellij.ide.util.DirectoryUtil;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.actionSystem.LangDataKeys;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.fileTypes.PlainTextFileType;
import com.intellij.openapi.project.DumbAwareAction;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import com.intellij.psi.PsiDirectory;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiFileFactory;
import com.intellij.psi.PsiFileSystemItem;
import com.jetbrains.python.PyNames;
import com.jetbrains.python.PythonFileType;
import com.jetbrains.python.psi.LanguageLevel;
import com.jetbrains.python.psi.PyElementGenerator;
import com.jetbrains.python.psi.PyExpression;
import com.jetbrains.python.psi.PyKeyValueExpression;
import icons.OdooIcons;
import org.buffagon.intellij.odoo.OdooConstants;
import org.buffagon.intellij.odoo.module.ModuleSections;
import org.buffagon.intellij.odoo.module.OdooModuleUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Действие для создания нового Odoo модуля.
 *
 * @author Прокофьев Алексей
 */
public class CreateOdooModuleAction extends DumbAwareAction {
  private static final Logger LOG = Logger.getInstance("#org.buffagon.intellij.odoo.actions.CreateOdooModuleAction");

  @Override
  public void actionPerformed(@NotNull AnActionEvent e) {
    final IdeView view = e.getData(LangDataKeys.IDE_VIEW);
    if (view == null) {
      return;
    }
    final Project project = e.getData(CommonDataKeys.PROJECT);
    final PsiDirectory directory = DirectoryChooserUtil.getOrChooseDirectory(view);
    if (directory == null) return;
    CreateDirectoryOrPackageHandler validator = new CreateDirectoryOrPackageHandler(project, directory, false, "") {
      @Override
      protected void createDirectories(String subDirName) {
        super.createDirectories(subDirName);
        PsiFileSystemItem element = getCreatedElement();
        if (element instanceof PsiDirectory)
          createOdooModuleStructure((PsiDirectory) element);
      }
    };
    Messages.showInputDialog(project, "Please inter new Odoo module name",
                             "New Odoo Module", OdooIcons.LOGO, "new_module", validator);
    final PsiFileSystemItem result = validator.getCreatedElement();
    if (result != null && result instanceof PsiDirectory) {
      PsiFile moduleFile = ((PsiDirectory) result).findFile(OdooConstants.ODOO_MODULE_FILENAME);
      if (moduleFile != null)
        view.selectElement(moduleFile);
      else
        view.selectElement(result);
    }
  }

  private void createOdooModuleStructure(PsiDirectory directory) {
    final PsiFile initFile = createPyFile(directory, PyNames.INIT_DOT_PY, "Python Script");
    fillInitFile(initFile);
    final PsiFile moduleFile = createPyFile(directory, OdooConstants.ODOO_MODULE_FILENAME, "Odoo Module");
    fillModuleFile(moduleFile);
    createPyFile(directory, directory.getName() + ".py", "Python Script");
    createSecurityDirectory(directory);
    createXmlViewFile(directory);
  }

  private static void createXmlViewFile(PsiDirectory directory) {
    final String filename = directory.getName() + "_view.xml";
    PsiFile file = createFileFromTemplate(directory, filename, "Odoo Xml View");
    if(file != null)
      return;

    file = PsiFileFactory.getInstance(directory.getProject()).createFileFromText(filename, XmlFileType.INSTANCE,
      "<?xml version=\"1.0\" ?>\n" +
        "<openerp>\n" +
        "    <data>\n" +
        "    </data>\n" +
        "</openerp>");
    directory.add(file);
  }

  private static void fillModuleFile(PsiFile moduleFile) {
    if (!FileModificationService.getInstance().prepareFileForWrite(moduleFile))
      return;
    PyKeyValueExpression keyValueExpression = OdooModuleUtil.findModuleSection(moduleFile, ModuleSections.DATA);
    if(keyValueExpression == null || keyValueExpression.getValue() == null)
      return;
    final PyElementGenerator generator = PyElementGenerator.getInstance(moduleFile.getProject());
    final PyExpression value = keyValueExpression.getValue();
    value.add(generator.createStringLiteralFromString(OdooConstants.SECURITY_DIR_NAME_AND_FILENAME));
    value.add(generator.createStringLiteralFromString(moduleFile.getContainingDirectory().getName() + "_view.xml"));
  }

  private static void fillInitFile(PsiFile initFile) {
    if (!FileModificationService.getInstance().prepareFileForWrite(initFile))
      return;
    final PyElementGenerator generator = PyElementGenerator.getInstance(initFile.getProject());
    initFile.add(generator.createFromImportStatement(
      LanguageLevel.getDefault(), ".", initFile.getContainingDirectory().getName(), null));
  }

  private static void createSecurityDirectory(PsiDirectory directory) {
    final PsiDirectory securityDir = DirectoryUtil.createSubdirectories(OdooConstants.SECURITY_DIR_NAME, directory, "");
    final PsiFile securityFile = PsiFileFactory.getInstance(directory.getProject()).createFileFromText(
      OdooConstants.SECURITY_CSV_FILENAME, PlainTextFileType.INSTANCE,
      "id,name,model_id/id,group_id/id,perm_read,perm_write,perm_create,perm_unlink\n");
    securityDir.add(securityFile);
  }

  @Nullable
  private static PsiFile createPyFile(PsiDirectory directory, String filename, String templateName) {
    PsiFile file = createFileFromTemplate(directory, filename, templateName);
    if(file != null)
      return file;
    file = PsiFileFactory.getInstance(directory.getProject()).createFileFromText(filename, PythonFileType.INSTANCE, "");
    directory.add(file);
    return file;
  }

  @Nullable
  private static PsiFile createFileFromTemplate(PsiDirectory directory, String filename, String templateName) {
    final FileTemplateManager fileTemplateManager = FileTemplateManager.getInstance();
    final FileTemplate template = fileTemplateManager.getInternalTemplate(templateName);
    if (directory.findFile(filename) != null)
      return null;
    if (template != null) {
      try {
        return (PsiFile) FileTemplateUtil.createFromTemplate(
          template, filename, fileTemplateManager.getDefaultProperties(directory.getProject()), directory);
      } catch (Exception e) {
        LOG.error(e);
      }
    }
    return null;
  }


  @Override
  public void update(@NotNull AnActionEvent e) {
    boolean enabled = isEnabled(e);
    e.getPresentation().setVisible(enabled);
    e.getPresentation().setEnabled(enabled);
  }

  private static boolean isEnabled(AnActionEvent e) {
    Project project = e.getData(CommonDataKeys.PROJECT);
    final IdeView ideView = e.getData(LangDataKeys.IDE_VIEW);
    if (project == null || ideView == null) {
      return false;
    }
    final PsiDirectory[] directories = ideView.getDirectories();
    return directories.length == 1 && directories[0].getVirtualFile().equals(project.getBaseDir());
  }
}
