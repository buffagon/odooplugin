package org.buffagon.intellij.odoo;

/**
 * Содержит наиболее часто используемые константы.
 *
 * @author Прокофьев Алексей
 */
public final class OdooConstants {
  public static final String ODOO_MODULE_FILENAME = "__openerp__.py";
  public static final String SECURITY_CSV_FILENAME = "ir.model.access.csv";
  public static final String SECURITY_DIR_NAME = "security";
  public static final String SECURITY_DIR_NAME_AND_FILENAME = SECURITY_DIR_NAME + "/" + SECURITY_CSV_FILENAME;

  public static final int DEFAULT_SERVER_PORT = 8069;
  public static final int DEFAULT_DB_PORT = 5432;
  public static final String MODEL_NAME_FIELD = "_name";
  public static final String MODEL_INHERIT_FIELD = "_inherit";
  public static final String MODEL_INHERITS_FIELD = "_inherits";
}
