package org.buffagon.intellij.odoo;

import com.intellij.openapi.options.Configurable;
import com.intellij.openapi.options.ConfigurationException;
import com.intellij.openapi.options.SearchableConfigurable;
import com.intellij.openapi.project.Project;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

/**
 * Регистрирует пункт меню "Odoo" в настройках проекта.
 *
 * @author Прокофьев Алексей
 */
public class OdooProjectSettingsConfigurable implements SearchableConfigurable, Configurable.NoScroll {
  private OdooProjectSettingsPanel myPanel;
  private final OdooProjectSettingsProvider mySettingsProvider;

  public OdooProjectSettingsConfigurable(Project project) {
    mySettingsProvider = OdooProjectSettingsProvider.getInstance(project);
  }

  @NotNull
  @Override
  public String getId() {
    return "odoo-project";
  }

  @Nullable
  @Override
  public Runnable enableSearch(String s) {
    return null;
  }

  @Nls
  @Override
  public String getDisplayName() {
    return "Odoo";
  }

  @Nullable
  @Override
  public String getHelpTopic() {
    return null;
  }

  @Nullable
  @Override
  public JComponent createComponent() {
    if (myPanel == null)
      myPanel = new OdooProjectSettingsPanel();
    return myPanel.getPanel(mySettingsProvider);
  }

  @Override
  public boolean isModified() {
    return myPanel.isModified();
  }

  @Override
  public void apply() throws ConfigurationException {
    myPanel.apply();
  }

  @Override
  public void reset() {
    myPanel.reset();
  }

  @Override
  public void disposeUIResources() {
    myPanel = null;
  }
}
