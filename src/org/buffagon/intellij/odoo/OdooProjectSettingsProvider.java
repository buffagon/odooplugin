/*
 * Copyright 2000-2013 JetBrains s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.buffagon.intellij.odoo;

import com.intellij.openapi.components.*;
import com.intellij.openapi.project.Project;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Профайдер настроек плагина.
 *
 * @author Прокофьев Алексей
 */
@State(name = "OdooProjectSettingsProvider", storages = {@Storage(file = StoragePathMacros.PROJECT_FILE)})
public class OdooProjectSettingsProvider implements PersistentStateComponent<OdooProjectSettingsProvider.State>, ProjectComponent {
  @Override
  public void projectOpened() {

  }

  @Override
  public void projectClosed() {

  }

  public static class State {
    public boolean myOdooEnabled = false;
    public String myConfigPath = "";
    public List<String> myAddonsPathList = new ArrayList<String>(0);
  }

  private State myState = new State();

  public static OdooProjectSettingsProvider getInstance(Project project) {
    return project.getComponent(OdooProjectSettingsProvider.class);
  }

  @Override
  public State getState() {
    return myState;
  }

  @Override
  public void loadState(State state) {
    myState.myAddonsPathList = new ArrayList<String>(state.myAddonsPathList);
    myState.myConfigPath = state.myConfigPath;
    myState.myOdooEnabled = state.myOdooEnabled;
  }

  public boolean isOdooEnabled() {
    return myState.myOdooEnabled;
  }

  public void setOdooEnabled(boolean enabled) {
    myState.myOdooEnabled = enabled;
  }

  public String getConfigPath() {
    return myState.myConfigPath;
  }

  public void setConfigPath(String path) {
    myState.myConfigPath = path;
  }

  public List<String> getAddonsPathList() {
    return myState.myAddonsPathList;
  }

  public void setAddonsPathList(List<String> addonsPathList) {
    myState.myAddonsPathList = addonsPathList;
  }

  @Override
  public void initComponent() {
  }

  @Override
  public void disposeComponent() {
  }

  @NotNull
  @Override
  public String getComponentName() {
    return "OdooProjectSettingsProvider";
  }
}

