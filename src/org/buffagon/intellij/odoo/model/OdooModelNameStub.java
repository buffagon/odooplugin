package org.buffagon.intellij.odoo.model;

import com.intellij.psi.stubs.StubOutputStream;
import com.intellij.psi.util.QualifiedName;
import com.jetbrains.python.psi.impl.stubs.CustomTargetExpressionStub;
import com.jetbrains.python.psi.impl.stubs.CustomTargetExpressionStubType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;

/**
 * Стаб для индексации наименований Odoo моделей.
 *
 * @author Прокофьев Алексей
 */
public class OdooModelNameStub implements CustomTargetExpressionStub {
  private String modelName;

  public OdooModelNameStub() {
    modelName = null;
  }

  public OdooModelNameStub(@NotNull String modelName) {
    this.modelName = modelName;
  }

  @NotNull
  @Override
  public Class<? extends CustomTargetExpressionStubType> getTypeClass() {
    return OdooModelNameStubType.class;
  }

  @Override
  public void serialize(StubOutputStream stream) throws IOException {
    if(modelName != null)
      stream.writeName(modelName);
  }

  @Nullable
  @Override
  public QualifiedName getCalleeName() {
    return null;
  }

  public String getModelName() {
    return modelName;
  }
}
