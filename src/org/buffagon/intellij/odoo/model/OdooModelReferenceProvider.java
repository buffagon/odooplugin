package org.buffagon.intellij.odoo.model;

import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiReference;
import com.intellij.psi.PsiReferenceProvider;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.util.ProcessingContext;
import com.jetbrains.python.psi.PyAssignmentStatement;
import com.jetbrains.python.psi.PyKeyValueExpression;
import com.jetbrains.python.psi.PyStringLiteralExpression;
import org.buffagon.intellij.odoo.OdooProjectSettingsProvider;
import org.jetbrains.annotations.NotNull;

/**
 * Провайдер ссылок для наименований Odoo моделей.
 *
 * @author Прокофьев Алексей
 */
public class OdooModelReferenceProvider extends PsiReferenceProvider {
  @NotNull
  @Override
  public PsiReference[] getReferencesByElement(@NotNull PsiElement element, @NotNull ProcessingContext context) {
    if(!(element instanceof PyStringLiteralExpression))
      return PsiReference.EMPTY_ARRAY;
    if(!OdooProjectSettingsProvider.getInstance(element.getProject()).isOdooEnabled())
      return PsiReference.EMPTY_ARRAY;
    PyAssignmentStatement assignmentStatement = PsiTreeUtil.getParentOfType(element, PyAssignmentStatement.class);
    if(assignmentStatement  == null)
      return PsiReference.EMPTY_ARRAY;
    if(OdooModelUtil.isModelInheritAttribute(assignmentStatement))
      return new PsiReference[]{new OdooModelReference((PyStringLiteralExpression) element)};
    if(OdooModelUtil.isModelInheritsAttribute(assignmentStatement)) {
      PyKeyValueExpression keyValueExpression = PsiTreeUtil.getParentOfType(element, PyKeyValueExpression.class);
      if(keyValueExpression != null && element.equals(keyValueExpression.getKey()))
        return new PsiReference[]{new OdooModelReference((PyStringLiteralExpression) element)};
    }
    return PsiReference.EMPTY_ARRAY;
  }
}
