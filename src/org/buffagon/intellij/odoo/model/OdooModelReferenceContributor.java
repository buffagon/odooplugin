package org.buffagon.intellij.odoo.model;

import com.intellij.psi.PsiReferenceContributor;
import com.intellij.psi.PsiReferenceProvider;
import com.intellij.psi.PsiReferenceRegistrar;
import com.jetbrains.python.patterns.PythonPatterns;
import org.jetbrains.annotations.NotNull;

/**
 * Нужен для регистрации провайдера ссылок наименований Odoo модедей.
 *
 * @author Прокофьев Алексей
 */
public class OdooModelReferenceContributor extends PsiReferenceContributor {
  @Override
  public void registerReferenceProviders(@NotNull PsiReferenceRegistrar registrar) {
    PsiReferenceProvider provider = new OdooModelReferenceProvider();
    registrar.registerReferenceProvider(PythonPatterns.pyLiteralExpression(), provider);
  }
}
