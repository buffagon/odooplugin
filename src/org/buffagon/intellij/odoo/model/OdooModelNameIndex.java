package org.buffagon.intellij.odoo.model;

import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.stubs.StringStubIndexExtension;
import com.intellij.psi.stubs.StubIndex;
import com.intellij.psi.stubs.StubIndexKey;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.util.ArrayUtil;
import com.jetbrains.python.psi.PyClass;
import com.jetbrains.python.psi.PyTargetExpression;
import com.jetbrains.python.psi.search.PyProjectScopeBuilder;
import org.buffagon.intellij.odoo.module.OdooModuleUtil;
import org.jetbrains.annotations.NotNull;

import java.util.*;

/**
 * Стаб-индекс по наименованиям Odoo моделей
 *
 * @author Прокофьев Алексей
 */
public class OdooModelNameIndex extends StringStubIndexExtension<PyTargetExpression> {
  public static final StubIndexKey<String, PyTargetExpression> KEY = StubIndexKey.createIndexKey("Odoo.model.name");

  @NotNull
  @Override
  public StubIndexKey<String, PyTargetExpression> getKey() {
    return KEY;
  }

  @NotNull
  public static String[] allKeys(@NotNull Project project) {
    return ArrayUtil.toStringArray(StubIndex.getInstance().getAllKeys(KEY, project));
  }

  public static Collection<PyTargetExpression> find(String name, Project project) {
    return find(name, project, true);
  }

  public static Collection<PyTargetExpression> find(String name, Project project, boolean includeNonProjectItems) {
    final GlobalSearchScope scope = getScope(project, includeNonProjectItems);
    return StubIndex.getElements(KEY, name, project, scope, PyTargetExpression.class);
  }

  public static List<PyTargetExpression> find(PsiElement reference, String name) {
    return find(reference, name, true);
  }

  public static List<PyTargetExpression> find(PsiElement reference, String name, boolean includeNonProjectItems) {
    final Project project = reference.getProject();
    // Получаем пространство для поиска
    final GlobalSearchScope scope = getScope(project, includeNonProjectItems);

    // Получаем файл описателя модуля для нашего элемента
    final PsiFile moduleFile = OdooModuleUtil.findModuleFileOfElement(reference);
    if (moduleFile == null)
      return new ArrayList<PyTargetExpression>(0);

    // Подготавливаем полный список зависимостей данного модуля
    final List<String> reachableModuleDepends = calcReachableModules(moduleFile);
    // добавляем название модуля
    reachableModuleDepends.add(moduleFile.getContainingDirectory().getName());

    // Найденные элементы в индексе + название их модулей
    final Map<PyTargetExpression, String> foundWithModule = new HashMap<PyTargetExpression, String>();
    for (PyTargetExpression expression : StubIndex.getElements(KEY, name, project, scope, PyTargetExpression.class)) {
      final String moduleName = OdooModuleUtil.findModuleName(expression);
      if (moduleName != null)
        foundWithModule.put(expression, moduleName);
    }

    // Пытаемся найти PyClass для переданного элемента
    final PyClass referenceClass = PsiTreeUtil.getParentOfType(reference, PyClass.class);

    // Добавляем только те результаты на которые есть зависимости
    List<PyTargetExpression> result = new ArrayList<PyTargetExpression>();
    for (Map.Entry<PyTargetExpression, String> entry : foundWithModule.entrySet()) {
      if (!reachableModuleDepends.contains(entry.getValue()))
        continue;
      if (referenceClass == null || !referenceClass.equals(PsiTreeUtil.getParentOfType(entry.getKey(), PyClass.class)))
        result.add(entry.getKey());
    }

    Collections.sort(result, new Comparator<PyTargetExpression>() {
      @Override
      public int compare(PyTargetExpression o1, PyTargetExpression o2) {
        final String key1 = foundWithModule.get(o1);
        final String key2 = foundWithModule.get(o2);
        return reachableModuleDepends.indexOf(key2) - reachableModuleDepends.indexOf(key1);
      }
    });
    return result;
  }

  @NotNull
  private static GlobalSearchScope getScope(@NotNull Project project, boolean includeNonProjectItems) {
    return includeNonProjectItems
      ? PyProjectScopeBuilder.excludeSdkTestsScope(project)
      : GlobalSearchScope.projectScope(project);
  }

  @NotNull
  private static List<String> calcReachableModules(PsiFile moduleFile) {
    final String moduleName = moduleFile.getContainingDirectory().getName();
    List<String> result = new ArrayList<String>();
    Stack<PsiFile> modulesStack = new Stack<PsiFile>();
    modulesStack.push(moduleFile);
    while (!modulesStack.isEmpty()) {
      final PsiFile currentModule = modulesStack.pop();
      final String currentName = currentModule.getContainingDirectory().getName();
      if(!result.contains(currentName) && !currentName.equals(moduleName))
        result.add(currentName);
      List<PsiFile> dependenceList = OdooModuleUtil.getModuleDepends(currentModule);
      if (dependenceList == null)
        continue;
      ListIterator<PsiFile> it = dependenceList.listIterator(dependenceList.size());
      while (it.hasPrevious())
        modulesStack.push(it.previous());
    }
    return result;
  }

  /**
   * Поиск всех названий моделей, видимых из reference
   */
  public static List<String> findModelNames(PsiElement reference, Project project,
                                                  boolean includeNonProjectItems) {
    List<String> result = new LinkedList<String>();
    PsiFile module =  OdooModuleUtil.findModuleFileOfElement(reference);
    final List<String> reachableModuleDepends = calcReachableModules(module);
    if (module != null) {
      reachableModuleDepends.add(module.getContainingDirectory().getName());
    }

    Collection<String> findResult = StubIndex.getInstance().getAllKeys(KEY, project);
    for (String key : findResult) {
      for (PyTargetExpression pyTargetExpression : find(key, project, includeNonProjectItems)) {
        //проверка на то, что модель с именем key является одним из reachableModuleDepends
        if (reachableModuleDepends.contains(OdooModuleUtil.findModuleName(pyTargetExpression))) {
          result.add(key);
        }
      }
    }
    return result;
  }


  /**
   * Поиск всех колонок модели name, видимой из reference
   */
  public static Set<String> findModelColumns(final PsiElement reference, String name, boolean includeNonProjectItems) {
    Collection<PyTargetExpression> models = findModelsIncludeInherits(reference, name, includeNonProjectItems);

    Set<String> result = new HashSet<String>();

    for (PyTargetExpression pyTargetExpression : models) {
      PyClass pyClass = PsiTreeUtil.getParentOfType(pyTargetExpression, PyClass.class);

      if (pyClass != null) {
        result.addAll(OdooModelUtil.findColumnNamesAtOsvModelClass(pyClass));
      }

    }

    return result;
  }

  /**
   * Находит все модели, включая указанные в _inherit и _inherits. Вспомогательный метод для функции findModelColumns
   */
  static Set<PyTargetExpression> findModelsIncludeInherits(final PsiElement reference, String name, boolean includeNonProjectItems) {
    Collection<PyTargetExpression> models = find(reference, name, includeNonProjectItems);
    Set<PyTargetExpression> inheritedModels = new HashSet<PyTargetExpression>();
    for (PyTargetExpression pyTargetExpression : models) {
      PyClass pyClass = PsiTreeUtil.getParentOfType(pyTargetExpression, PyClass.class);
      Collection<String> inherits = OdooModelUtil.findInherits(pyClass);
      for (String inherit : inherits) {
        if (inherit.equals(name))
          continue;
        final Set<PyTargetExpression> childResult =
          findModelsIncludeInherits(reference, inherit, includeNonProjectItems); // TODO: убрать рекурсию
        if(!childResult.isEmpty())
          inheritedModels.addAll(childResult);
      }
    }
    inheritedModels.addAll(models);
    return inheritedModels;
  }
}
