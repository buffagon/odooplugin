package org.buffagon.intellij.odoo.model;

import com.intellij.psi.*;
import com.jetbrains.python.psi.PyStringLiteralExpression;
import com.jetbrains.python.psi.PyTargetExpression;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

/**
 * Обеспечивает переход по ссылке на наименование Odoo модели.
 *
 * @author Прокофьев Алексей
 */
public class OdooModelReference extends PsiReferenceBase<PyStringLiteralExpression> implements PsiPolyVariantReference {
  public OdooModelReference(@NotNull PyStringLiteralExpression element) {
    super(element);
  }

  @NotNull
  @Override
  public ResolveResult[] multiResolve(boolean incompleteCode) {
    Collection<PyTargetExpression> expressions = OdooModelNameIndex.find(getElement(),getElement().getStringValue());
    ResolveResult[] result = new ResolveResult[expressions.size()];
    int i = 0;
    for(PyTargetExpression expression : expressions)
      result[i++] = new PsiElementResolveResult(expression);
    return result;
  }

  @Nullable
  @Override
  public PsiElement resolve() {
    return null;
  }

  @NotNull
  @Override
  public Object[] getVariants() {
    return new Object[0];
  }
}
