package org.buffagon.intellij.odoo.model;

import com.intellij.codeInsight.completion.*;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.project.DumbService;
import com.intellij.openapi.project.Project;
import com.intellij.patterns.PlatformPatterns;
import com.intellij.psi.PsiElement;
import com.intellij.psi.stubs.StubIndex;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.util.ProcessingContext;
import com.jetbrains.python.psi.PyAssignmentStatement;
import com.jetbrains.python.psi.PyKeyValueExpression;
import com.jetbrains.python.psi.PyStringLiteralExpression;
import org.buffagon.intellij.odoo.OdooProjectSettingsProvider;
import org.jetbrains.annotations.NotNull;

/**
 * Класс предоставляет автодополнение для файла-описателя модуля.
 *
 * @author Прокофьев Алексей
 * @version 1.0.0
 */
public class OdooModelNameCompletionContributor extends CompletionContributor {
  public OdooModelNameCompletionContributor() {
    extend(CompletionType.BASIC,
           PlatformPatterns.psiElement().withParent(PyStringLiteralExpression.class),
           new CompletionProvider<CompletionParameters>() {
             @Override
             protected void addCompletions(@NotNull CompletionParameters parameters, ProcessingContext context,
                                           @NotNull CompletionResultSet result) {
               final Project project = parameters.getOriginalFile().getProject();
               // проверяем что плагин включен
               if(!OdooProjectSettingsProvider.getInstance(project).isOdooEnabled())
                 return;
               // проверяем что не идет построение индекса
               DumbService dumbService = DumbService.getInstance(project);
               if(dumbService.isDumb())
                 return;
               PyAssignmentStatement assignmentStatement =
                 PsiTreeUtil.getParentOfType(parameters.getPosition(), PyAssignmentStatement.class);
               if(assignmentStatement != null) {
                 if(OdooModelUtil.isModelInheritAttribute(assignmentStatement)) {
                   addModelNames(result, project);
                 }else if(OdooModelUtil.isModelInheritsAttribute(assignmentStatement)) {
                   PsiElement parent = parameters.getPosition().getParent();
                   PyKeyValueExpression keyValueExpression =
                     PsiTreeUtil.getParentOfType(parent, PyKeyValueExpression.class);
                   if(keyValueExpression == null)
                     addModelNames(result, project);
                   if(keyValueExpression != null && parent.equals(keyValueExpression.getKey()))
                     addModelNames(result, project);
                 }
               }
             }
           }
    );
  }

  private void addModelNames(@NotNull final CompletionResultSet result, final Project project) {
    ApplicationManager.getApplication().runReadAction(new Runnable() {
      @Override
      public void run() {
        for(String name: StubIndex.getInstance().getAllKeys(OdooModelNameIndex.KEY, project)) {
          result.addElement(LookupElementBuilder.create(name));
        }
      }
    });
  }
}
