package org.buffagon.intellij.odoo.model;

import com.intellij.psi.PsiElement;
import com.intellij.psi.util.PsiTreeUtil;
import com.jetbrains.python.psi.*;
import org.buffagon.intellij.odoo.OdooConstants;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Вспомогательный класс для работы с Odoo моделями.
 *
 * @author Прокофьев Алексей
 */
public final class OdooModelUtil {
  public static boolean isModelNameAttribute(@NotNull PyAssignmentStatement psi) {
    return isModelAttribute(psi, OdooConstants.MODEL_NAME_FIELD);
  }

  public static boolean isModelInheritAttribute(@NotNull PyAssignmentStatement psi) {
    return isModelAttribute(psi, OdooConstants.MODEL_INHERIT_FIELD);
  }

  public static boolean isModelInheritsAttribute(@NotNull PyAssignmentStatement psi) {
    return isModelAttribute(psi, OdooConstants.MODEL_INHERITS_FIELD);
  }

  public static boolean isModelAttribute(@NotNull PyAssignmentStatement psi, @NotNull String attribute) {
    PsiElement[] children = psi.getChildren();
    return children.length == 2 && children[0] instanceof PyTargetExpression
      && attribute.equals(((PyTargetExpression) children[0]).getName())
      && psi.getParent() instanceof PyStatementList
      && psi.getParent().getParent() instanceof PyClass;
  }

  /**
   * Получить поля (колонки) Odoo-модели
   */
  public static Set<String> findColumnNamesAtOsvModelClass(PyClass pyClass) {
    Set<String> columnNames = new HashSet<String>();
    final Collection<PyAssignmentStatement> statements =
      PsiTreeUtil.findChildrenOfType(pyClass, PyAssignmentStatement.class);
    for (PyAssignmentStatement statement : statements) {
      PsiElement columns = statement.getElementNamed("_columns");
      if(!(columns instanceof PyTargetExpression))
        continue;       
      PyExpression dict = statement.getAssignedValue();
      if(!(dict instanceof PyDictLiteralExpression))
        continue;        

      for (PyKeyValueExpression column : ((PyDictLiteralExpression) dict).getElements()) {
        if(column.getKey() instanceof StringLiteralExpression)
          columnNames.add(((StringLiteralExpression) column.getKey()).getStringValue());
      }
    }
    return columnNames;
  }

  public static Set<String> findInherits(final PyClass pyClass) {
    Set<String> result = new HashSet<String>();
    for (PyAssignmentStatement statement : PsiTreeUtil.findChildrenOfType(pyClass, PyAssignmentStatement.class)) {
      if (isModelInheritsAttribute(statement)) {
        PyExpression dict = statement.getAssignedValue();
        if(!(dict instanceof PyDictLiteralExpression))
          continue;
        for (PyKeyValueExpression expression : ((PyDictLiteralExpression) dict).getElements())
          if(expression.getKey() instanceof StringLiteralExpression)
            result.add(((StringLiteralExpression) expression.getKey()).getStringValue());
      }

      if (isModelInheritAttribute(statement)) {
        PyExpression expression = statement.getAssignedValue();
        if(expression instanceof StringLiteralExpression)
          result.add(((StringLiteralExpression) expression).getStringValue());
      }
    }
    return result;
  }

}
