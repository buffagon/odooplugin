package org.buffagon.intellij.odoo.model;

import com.intellij.psi.stubs.IndexSink;
import com.intellij.psi.stubs.StubInputStream;
import com.intellij.util.io.StringRef;
import com.jetbrains.python.psi.*;
import com.jetbrains.python.psi.impl.stubs.CustomTargetExpressionStubType;
import com.jetbrains.python.psi.stubs.PyTargetExpressionStub;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;


/**
 * Расширение необходимое для формирования пользовательских стаб-индексов.
 *
 * @author Прокофьев Алексей
 */
public class OdooModelNameStubType extends CustomTargetExpressionStubType<OdooModelNameStub> {
  @Nullable
  @Override
  public OdooModelNameStub createStub(PyTargetExpression psi) {
    if(psi.getParent() instanceof PyAssignmentStatement
      && OdooModelUtil.isModelNameAttribute((PyAssignmentStatement) psi.getParent())) {
      PyExpression expression = psi.findAssignedValue();
      if((expression instanceof PyStringLiteralExpression))
        return new OdooModelNameStub(((PyStringLiteralExpression) expression).getStringValue());
    }
    return null;
  }

  @Nullable
  @Override
  public OdooModelNameStub deserializeStub(StubInputStream stream) throws IOException {
    StringRef modelName = stream.readName();
    if(modelName != null)
      return new OdooModelNameStub(modelName.getString());
    return null;
  }

  @Override
  public void indexStub(PyTargetExpressionStub stub, IndexSink sink) {
    OdooModelNameStub customStub = stub.getCustomStub(OdooModelNameStub.class);
    if(customStub != null)
      sink.occurrence(OdooModelNameIndex.KEY, customStub.getModelName());
  }
}
